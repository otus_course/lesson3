package ru.rt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Primes implements Task {
    int n;
    int q;
    int count = 0;
    int primes[];
    @Override
    public String run(String[] data) {
       return run3(data);
    }

    private String run1(String[] data) {
        int n = Integer.valueOf(data[0]);
        int q = 0;

        for (int i = 2; i <= n; i++) {
            if(isPrime7(i)){
                q++;
            }
        }
        System.out.println(q);
        return String.valueOf(q);
    }

    public String run2(String[] data) {
        int n = Integer.valueOf(data[0]);
        int count = 0;
        primes = new int[n];
        primes[count++] = 2;

        for (int i = 3; i <= n; i+=2) {
            if(isPrimeCount1(i)){
                primes[count++] = i;
            }
        }
        System.out.println(count);
        return String.valueOf(count);
    }

    public String run3(String[] data) {
        n = Integer.valueOf(data[0]);
        return String.valueOf(eratosphenOptimized());
    }

    public int eratosphen() {
        boolean[] divs = new boolean[n+1];
        count = 0;
        int sqrt = (int)Math.sqrt(n);
        for (int j = 2; j < n; j++) {
            if(!divs[j]){
                count++;
                if(j<=sqrt)
                for (int k = j*j; k <= n; k+=j) {
                    divs[k] = true;
                }
            }
        }
        return count;
    }

    public int eratosphenOptimized() {
        List<Integer> lp = new ArrayList<>(Collections.nCopies(n+1, 0));
        List<Integer> pr = new ArrayList<>();
        count=0;
        for (int i = 2; i <= n; i++) {
            if(lp.get(i).equals(0)){
                lp.set(i,i);
                pr.add(i);
                count++;
            }
            for (int p : pr) {
                if(p<=lp.get(i) && p*i > 0 && p*i<=n){
                    lp.set(i*p,p);
                }
            }
        }
        return count;
    }

    public boolean isPrimeCount1(int i) {
        int sqrt = (int)Math.sqrt(i);
        for (int j = 0; primes[j] <= sqrt; j++) {
            if(i % primes[j] == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isPrime1(int i) {
        int count = 0;
        for (int j = 1; j <= i; j++) {
            if(i%j == 0){
                count++;
            }

        }
        return count == 2;
    }

    public boolean isPrime2(int i) {
        for (int j = 2; j < i; j++) {
            if(i%j == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isPrime3(int i) {
        for (int j = 2; j <= i/2; j++) {
            if(i%j == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isPrime4(int i) {
        for (int j = 2; j <= Math.sqrt(i); j++) {
            if(i%j == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isPrime5(int i) {
        if(i == 2) return true;
        if(i % 2 == 0) return false;
        for (int j = 3; j <= Math.sqrt(i); j+=2) {
            if(i%j == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isPrime6(int i) {
        if(i == 2) return true;
        if(i % 2 == 0) return false;
        int sqrt = (int)Math.sqrt(i);
        for (int j = 3; j <= sqrt; j+=2) {
            if(i%j == 0){
                return false;
            }
        }
        return true;
    }

    public boolean isPrime7(int i) {
        if(i == 2) return true;
        if(i == 3) return true;
        if(i % 2 == 0) return false;
        if(i % 3 == 0) return false;
        int sqrt = (int)Math.sqrt(i);
        for (int j = 5; j <= sqrt; j+=6) {
            if(i%j == 0){
                return false;
            }
            if(i%(j+2) == 0){
                return false;
            }
        }
        return true;
    }
}
