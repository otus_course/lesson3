package ru.rt;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Power implements Task{
    BigDecimal a;
    long n;

    @Override
    public String run(String[] data) {
        a = BigDecimal.valueOf(Double.valueOf(data[0]));
        n = Long.valueOf(data[1]);
        return power2();
    }

    private String power1() {
        BigDecimal p = new BigDecimal(1);
        for (int i = 1; i <= n; i++) {
            p = p.multiply(a);
        }
        p = p.setScale(11, RoundingMode.HALF_UP);
        return String.valueOf(p.doubleValue());
    }

    private String power2() {
        BigDecimal p = new BigDecimal(1);
        BigDecimal b = a;
        int i = 1;
        while (true) {
            if(i*2>n)break;
            i*=2;
            b = b.multiply(b);
            p=b;
        }
        for (int j = i; j < n; j++) {
            p = p.multiply(a);
        }
        p = p.setScale(11, RoundingMode.HALF_UP);
        System.out.println(p.toString());
        return String.valueOf(p.doubleValue());
    }


}
