package ru.rt;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Fibo implements Task{
    BigDecimal n;
    @Override
    public String run(String[] data) {
        n = BigDecimal.valueOf(Long.valueOf(data[0]));
        BigDecimal res = matrix();
        System.out.println(res.toString());
        return res.toString();
    }

    private BigDecimal fibo1(BigDecimal n) {
        if(n.compareTo(new BigDecimal(1))<=0){
            return n;
        }
        return fibo1(n.subtract(new BigDecimal(1))).add(fibo1(n.subtract(new BigDecimal(2))));
    }

    private BigDecimal fibo2() {
        BigDecimal a = new BigDecimal(0);
        BigDecimal b = new BigDecimal(1);
        BigDecimal c = n;
        for (int i = 1; i < n.longValue(); i++) {
            c = a.add(b);
            a=b;
            b=c;
        }
        return c;
    }

    private BigDecimal fibo3() {
        BigDecimal sqrt5 = new BigDecimal(5).sqrt(new MathContext(11,RoundingMode.HALF_UP));
        BigDecimal fi = new BigDecimal(1)
                .add(sqrt5)
                .divide(new BigDecimal(2));

        BigDecimal res = fi.pow(n.intValue())
                            .divide(sqrt5, RoundingMode.HALF_UP)
                            .add(new BigDecimal(0.5));
        return res.setScale(0, RoundingMode.HALF_UP);
    }


    private BigDecimal matrix() {
        if(n.intValue() <= 1)
            return n;

        if(n.intValue() == 2)
            return new BigDecimal(1);

        BigDecimal[][] base = new BigDecimal[][]{{new BigDecimal(1),new BigDecimal(1)},
                                                    {new BigDecimal(1),new BigDecimal(0)}};
        BigDecimal[][] res = base;
        for (int i = 1; i < (n.intValue()-1); i++) {
            res = matrixMultiply(res, base);
        }


        return res[0][0];
    }

    private BigDecimal[][] matrixMultiply(BigDecimal[][] matr1, BigDecimal[][] matr2) {
        return new BigDecimal[][]
                {{matr1[0][0].multiply(matr2[0][0]).add(matr1[0][1].multiply(matr2[1][0])), matr1[0][0].multiply(matr2[0][1]).add(matr1[0][1].multiply(matr2[1][1]))},
                    {matr1[1][0].multiply(matr2[0][0]).add(matr1[1][1].multiply(matr2[1][0])), matr1[1][0].multiply(matr2[0][1]).add(matr1[1][1].multiply(matr2[1][1]))}};
    }

}
